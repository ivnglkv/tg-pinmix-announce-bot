#!/usr/bin/env python3

from tornado import ioloop, web, httpserver
import os

import settings
from url_handlers import WebhookHandler, MainHandler


def main():
    http_settings = dict(
        ssl_options={
            "certfile": os.path.join(settings.LETSENCRYPT_PATH, "cert.pem"),
            "keyfile": os.path.join(settings.LETSENCRYPT_PATH, "privkey.pem"),
            "ca_certs": os.path.join(settings.LETSENCRYPT_PATH, "chain.pem"),
        },
    )

    handlers = [
            (r'/', MainHandler),
            (r'/webhook-{}'.format(settings.TOKEN), WebhookHandler),
    ]

    http_server = httpserver.HTTPServer(web.Application(handlers), **http_settings)

    http_server.listen(8443)
    ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()
