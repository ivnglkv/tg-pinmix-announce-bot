from os import environ

LETSENCRYPT_PATH = '/etc/letsencrypt/live/ivnglkv.ru'
TOKEN = environ['TELEGRAM_PINMIX_BOT_TOKEN']
API_URL = 'https://api.telegram.org/bot{}/'.format(TOKEN)

CMD = {
    '/subscribe': 'подписаться на обновления',
    '/unsubscribe': 'отписаться от обновлений',
    '/info': 'вывести информацию о подписках',
}
