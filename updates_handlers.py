import settings


class UpdateHandler:
    def __init__(self, update):
        self.update_id = update['update_id']
        self.answer = dict()
        self.sender_id = None
        self.sender_username = None

        for item in update:

            try:
                if 'from' in update[item]:
                    self.sender_id = update[item]['from']['id']
                    self.sender_username = update[item]['from']['username']

                    break
            except Exception:
                continue

    def process_update(self):
        """
        Общий метод для обработчиков входящих событий
        Конечная цель -- формирование словаря self.answer
        для отправки в ответ на входящее действие
        """
        pass


class CommandHandler(UpdateHandler):
    def __init__(self, update):
        super().__init__(update)

        msg = update['message']

        # находим команду среди элементов сообщения
        cmd_entity = None
        for entity in msg['entities']:
            if entity['type'] == 'bot_command':
                cmd_entity = entity

                break

        if not cmd_entity:
            raise Exception

        cmd_start = cmd_entity['offset']
        cmd_length = cmd_entity['length']

        self.cmd = msg['text'][cmd_start:cmd_start+cmd_length]

    def process_update(self):
        super().process_update()

        response_text = ''

        if self.cmd == '/start':
            response_text = '' \
                'Привет, @{}!\n' \
                'Я буду присылать тебе скелет для анонса новых маршрутов в Telegram, если ты ' \
                'отправишь мне команду /subscribe' \
                ''.format(self.sender_username)
        elif self.cmd == '/help':
            result = ['Ты общаешься с ботом, следящим за новыми темами '
                      'в группах PIN-MIX и ПинОрг.\n'
                      'Пока что я научен только этим командам:']

            for command in settings.CMD:
                result.append('{} — {}'.format(command, settings.CMD[command]))

            response_text = '\n    '.join(result)
        elif self.cmd == '/subscribe':
            # TODO: реализовать клавиатуру с выбором варианта подписки
            response_text = 'Заглушка'
        elif self.cmd == '/unsubscribe':
            # TODO: реализовать клавиатуру с выбором, от чего отписываться
            response_text = 'Заглушка'
        elif self.cmd == '/info':
            # TODO: реализовать вывод активных подписок
            response_text = 'У вас нет активных подписок'

        self.answer = {
            'method': 'sendMessage',
            'chat_id': self.sender_id,
            'text': response_text,
        }


class QueryHandler(UpdateHandler):
    def __init__(self, update):
        super().__init__(update)
