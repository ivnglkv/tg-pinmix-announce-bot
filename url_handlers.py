from tornado import escape, web
from datetime import datetime

from updates_handlers import UpdateHandler, CommandHandler, QueryHandler
# import settings


class MainHandler(web.RequestHandler):
    def get(self):
        self.write("Hello, world!")


class WebhookHandler(web.RequestHandler):
    def get(self):
        self.write('Handler!')

    def post(self):
        print('Got request {} at {}'.format(self.request.body, datetime.now()))
        update = escape.json_decode(self.request.body)

        update_handler = None

        if 'message' in update:
            update_handler = CommandHandler(update)
        elif 'callback_query' in update:
            update_handler = QueryHandler(update)
        else:
            update_handler = UpdateHandler(update)

        update_handler.process_update()

        if update_handler.answer:
            self.set_header('Content-Type', 'application/json')
            print('Formed answer at {}'.format(datetime.now()))

            self.write(update_handler.answer)

            print('Send response {} at {}'.format(update_handler.answer, datetime.now()))
        else:
            self.write('Ok')
